package com.onquery.filters;

interface Filter {
	public function match(object:Dynamic):Bool;
}